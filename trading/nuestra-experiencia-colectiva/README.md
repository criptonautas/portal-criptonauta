# Nuestra experiencia colectiva

!!!contrast
By replacing fear of the unknown with curiosity we open ourselves up to an infinite stream of possibility.

— Alan Watts
!!!

Hola terrícola! 👋

Te escribe **matenauta** pero junto a la comunidad que se sumó a editar y colaborar libremente en nuestro Portal.

**Esto es el resultado de prácticamente toda mi vida, porque el trading motiva y exige todo lo que me hace bien.**

Entenderás que elegir sobre nuestro dinero implica un grado de libertad y equilibrio que ni siquiera imaginamos desde lo que aquí definimos como "La Matrix".

> _"La Matrix" también puede ser definida como "sociedad", "capitalismo" o "sistema"._
>
> _Nos interesa el concepto lejos de sus connotaciones políticas o ideológicas._

## Por qué aprendí trading

Estudié y me desarrollé profesionalmente en el trading de criptomonedas para costear el nivel de vida que disfruto sin entregar 1/3 de mi tiempo (o incluso más, contando horas _extras_)...

Y sin responsabilidades o generando dependencia sobre terceros.

Por otra parte, me propuse mezclar música electronica sin depender de ello para sostener mi economía.

Y tal vez te preguntes por qué te cuento esto cuando solo quieres hacer dinero!

Entonces me permito acercarte un puñetazo de realidad:

**Si no encuentras un propósito mayor a hacer dinero en tu trading, hay muchísimas chances de que lo pierdas.**

Porque nos pasó a todos los que pensamos que el trading es una máquina de hacer dinero.

Así funcionan las cosas a nivel universal, generamos lo opuesto a lo que intentamos forzar.

## La realidad supera a la ficción

La idea de lo incierto cotidianamente nos asusta.

Esperamos controlar todas las cosas, cuando no podemos ni siquiera con nuestros propios pensamientos, sentimientos o emociones.

Y lo siento si esperabas algo distinto.

**Pero te sentirás incómod@ a lo largo de esta experiencia, si esperas resolver tu economía sin esfuerzo, o de un día para el otro.**

Aquí podrás aprender practicando y compartiendo pero también perdiendo mucho dinero, si decides hacer las cosas a tu modo y salteas cada una de nuestras sugerencias y recomendaciones (serás libre de hacerlo, todo el tiempo).

FOTOPROFITDISCORDCOMUNIDAD

Aprenderás de manera amigable o muy dura, porque nuestro único y principal objetivo es que aprendas y te liberes.

Entenderás por qué este Portal no es técnicamente un libro ni tampoco un curso, y así descubrirás que el trading en realidad es nuestro estilo de vida 🧡

!!!info Atenti

**Solo debes continuar con anotador en mano para anotar lo que no entiendas!**

!!!

## **Validamos el trading entre todos**

Porque a pesar de esta foto:

![El conocimiento no entra desde caras o fotos "serias"](https://user-images.githubusercontent.com/98671738/212181424-d06ee8b0-be76-40e9-bb4e-8074f348b44e.png)

No soy ni seré un gurú, porque no me interesó, interesa ni interesará serlo.

> _Los gurúes "comparten" (leen guiones ajenos) cierta información útil, pero su razón de ser no es compartir información, sino vender servicios inútiles, a partir del marketing más mercenario que existe._

Tuve que anunciar mi despedida de un servicio online que llegó a cientos de miles de personas, luego de mencionarles que iban a perder todo su capital si no cerraban sus posiciones.

_En ese sistema no estaba contemplada la opción de salir, y por eso "me salí yo" junto a quienes recibieron mi sugerencia y recomendación de hacerlo cuanto antes._

_FOTOWEBINARBUSCARENS3(matenauta)_

### Allí logramos un 574% en menos de 6 meses

FOTOPROFITSIG

Multiplicamos 5 veces el dinero invertido en medio año, y fueron presentes de ese resultado millones de personas en todo el mundo, que recibían publicidad en redes sociales disfrazadas de "clases gratuitas".

_Lo hicimos de manera descentralizada, y eso fue impensado para una empresa como la que me cuasi contrató, pero una vez más confirmamos que las excepciones existen_ 😬

**El trading rompe muchísimos esquemas**, sobre todo en lo instalado por la "economía clásica" que nos programa para ahorrar algo que se devalúa a un ritmo exponencialmente creciente (cada vez mayor).

![El dólar compra cada vez menos cosas](https://user-images.githubusercontent.com/98671738/210263652-4257423d-f7c0-4e1a-916a-7031bad18f05.png)

**Aprendí que el marketing de gurúes es lo que mejor funciona con el 97% que se mueve por emociones que no aprendió a gestionar.**

Y desde ellos se valida una figura conocida como "falacia de autoridad" que logra récords en speechs de ventas mezclados con datos económicos reciclados o hiper trillados.

[!ref icon="info" target="blank" text="Argumento Ad Verecundiam"]([https://es.wikipedia.org/wiki/Argumento\_ad\_verecundiam])

## El trading es honesto

Porque no es certero ni preciso pero sabemos eso _desde el vamos_ y entendemos que da igual quiénes seamos las personas, cuando lo que importa es lo que transmitimos o compartimos con el resto.

El trading no se trata de mí, de Steve Nison ni mucho menos de Robert Kiyos\*\*\*.

**Se trata de conocer cada una de nuestras limitaciones y aprender a convivir con todos nuestros miedos y deseos más profundos.**

Es lo más real que experimentamos y vivimos quienes lo estudiamos, practicamos y aplicamos a nuestra vida. Y aquí lo entendemos como algo colectivo, no individual.

> Por eso desarrollamos la primera etapa del curso de manera holística y junto a \~300 personas hispano-hablantes durante dos años.

FOTOARCHIVOCARASVIDEOJUNTADA

**Aprendí mucho estudiando, pero más practicando y acompañando a otros en su aprendizaje.**

Las frases bonitas no generan traders sino nuestra práctica constante en comunidad. Y eso es lo que hacemos en Criptonautas.

## Todos regalamos algo de dinero

Y entendemos que eso también es parte de nuestro aprendizaje.

![¿Aprende TRADING en SOLO 10 MINUTOS?](../../.gitbook/assets/imagen.png)

Youtube es un nido de estafas e info basura pero nadie te mandó a meterte ahí adentro

**¿Cuántos youtubers crees que son traders?**

Claro, pensamos que la suerte cambiará nuestra realidad y perseguimos durante toda la vida ilusiones. Por eso muchos siguen anotando su número en la lotería cada semana.

Puedes tener suerte y hacer una diferencia importante tradeando alguna memecoin o comprando estafas como LUNA...

![image](https://user-images.githubusercontent.com/122026745/210851418-7b5b788a-fef4-43a1-b8d9-366f9793cf1a.png)

Pero creeme, siempre dura poco:

![image](https://user-images.githubusercontent.com/122026745/210851635-82422f92-c6c9-453f-b769-28e3dc1817f3.png)

Y de nada sirve ganar mucho dinero para luego perderlo de una vez. Porque **el trading es mucho más que juntar indicadores y fijar un \_stop-loss**\_**.**

> _“Decide si es un pasatiempo o un trabajo. Si es un pasatiempo, mejor encuentra otro porque este va a resultar muy caro y peligrosamente adictivo”._
>
> — Al Brooks

## Nuestro método educativo es meritorio

_¿Te preguntaste por qué no nos enseñan a plantar, cultivar, alimentarnos, hacer yoga, gestionar emociones, nuestro dinero o meditar?_

Es absurdo que un profesor puntúe a sus alumnos en base a lo que repitan (o no) y según su criterio.

Por eso no reconocemos autoridades sino argumentos, y sumamos a nuestro desarrollo niveles de compromiso que mantienen la información en tres etapas.

Porque **el objetivo de la comunidad en este curso es que la mayoría aprenda trading**.

Esta es la **primera Fase del Portal** y mi regalo hacia la comunidad de traders y [Cypherpunks](https://en.wikipedia.org/wiki/Cypherpunk).

_Eso incluye a cualquiera que respete y valore el open-source, la libertad (privacidad) sobre todas las cosas, y/o se anime a hacer lo que quiera con su vida_ 👊
